package com.example.umtenew;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.example.umtenew.utils.Prefs;

public class SettingActivity extends AppCompatActivity {

    Switch aSwitch;
    Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        aSwitch = findViewById(R.id.darkMode);
        if (prefs.loadNightMode()){
           aSwitch.setChecked(true);
        }
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    prefs.setNightMode(true);
                    recreate();
                }else {
                    prefs.setNightMode(false);
                    recreate();
                }
            }
        });

    }

}
