package com.example.umtenew;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText emailId, password, userName;
    Button btnSignUp, loginBtn,registerBtn;
    TextView tvSignInl;
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore firestore;
    FirebaseDatabase database;
    private SignInButton signInButton;
    private GoogleSignInClient googleSignInClient;
    private String TAG = "MainActivity";
    private int RC_SIGN_IN = 1;
    private String userID;
    DatabaseReference ref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        emailId = findViewById(R.id.email);
        password = findViewById(R.id.password);
        btnSignUp = findViewById(R.id.register);
        signInButton = findViewById(R.id.signInGoogle);
        tvSignInl = findViewById(R.id.textView);
        userName = findViewById(R.id.userName);
        loginBtn = findViewById(R.id.loginTabButton);
        registerBtn = findViewById(R.id.registerTabButton);

        registerBtn.setBackgroundColor(Color.GRAY);

        mFirebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        database = FirebaseDatabase.getInstance();
        ref = database.getReference("users");

        GoogleSignInOptions inOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent initLogin = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(initLogin);
            }
        });

        googleSignInClient = GoogleSignIn.getClient(this, inOptions);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = emailId.getText().toString();
                String pwd = password.getText().toString();
                final String userNm = userName.getText().toString();
                if (email.isEmpty()) {
                    emailId.setError("Prosim vlozte email");
                    emailId.requestFocus();
                } else if (pwd.isEmpty()) {
                    password.setError("Prosim vlozte heslo");
                    password.requestFocus();
                } else if (pwd.length() < 6) {
                    password.setError("Heslo musi mit minimalne 6 znaku");
                    password.requestFocus();
                } else if (email.isEmpty() && pwd.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Pole jsou prazdna", Toast.LENGTH_SHORT).show();

                } else if (!(email.isEmpty() && pwd.isEmpty())) {
                    mFirebaseAuth.createUserWithEmailAndPassword(email, pwd).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                    Toast.makeText(MainActivity.this, "Tato adresa je jiz pouzivana", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(MainActivity.this, "Zkuste to prosim později", Toast.LENGTH_SHORT).show();
                                                                    }

                            } else {
                                userID = mFirebaseAuth.getCurrentUser().getUid();
                                DocumentReference reference = firestore.collection("users").document(userID);


                                DatabaseReference usersRef = ref.child(userID);


                                Map<String,Object> user = new HashMap<>();
                                user.put("id",userID);
                                user.put("userName", userNm);
                                user.put("email",email);

                                usersRef.setValue(user);

                                reference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                    }
                                });

                                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                            }
                        }
                    });


                } else {
                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tvSignInl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
    }

    private void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> accountTask = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(accountTask);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> accountTask) {
        try {
            GoogleSignInAccount account = accountTask.getResult(ApiException.class);
            FireBaseGoogleAuth(account);

        } catch (ApiException e) {
            Toast.makeText(MainActivity.this, "Zkuste to prosim později", Toast.LENGTH_SHORT).show();
            FireBaseGoogleAuth(null);
        }
    }

    private void FireBaseGoogleAuth(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Prihlaseno uspesne", Toast.LENGTH_SHORT).show();
                    FirebaseUser user = mFirebaseAuth.getCurrentUser();

                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                } else {

                }

            }
        });
    }
}
