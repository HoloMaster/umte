package com.example.umtenew;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.umtenew.utils.Picture;
import com.example.umtenew.utils.TranslateWeather;
import com.example.umtenew.utils.Weather;
import com.example.umtenew.utils.WeatherIconResolver;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.example.umtenew.HomeActivity.friends;
import static com.example.umtenew.HomeActivity.pictures;

public class MainFragment extends Fragment {

    private TextView degrees, town, weatherIconDesc;
    private ImageView icon;
    private Weather weather;
    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();


    @Override
    public void onStart() {
        super.onStart();
        searchForFriends();
        if (friends != null && !friends.isEmpty()) {
           // searchForPicturesOfFriendsAndItself();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main,container,false);
        HomeActivity activity = (HomeActivity) getActivity();
        double [] coords = activity.getLocation();
        if (coords != null){
            weather = activity.getWeather(coords[0],coords[1]);
        }
        searchForFriends();
        if (friends != null && !friends.isEmpty()) {
          //  searchForPicturesOfFriendsAndItself();
        }

        if (weather != null) {

            degrees = view.findViewById(R.id.degree);
            degrees.setText(weather.getDegrees().toString());

            town = view.findViewById(R.id.town);
            town.setText(weather.getTown());

            weatherIconDesc = view.findViewById(R.id.weatherIconDesc);
            weatherIconDesc.setText(TranslateWeather.getTranslatedToCz(weather));

            icon = view.findViewById(R.id.icon);
            icon.setImageResource(WeatherIconResolver.getIconsForCurrWeather(weather.getIcon()));
            icon.setColorFilter(Color.BLACK);

        }

        return view;
    }

    private void searchForFriends() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Friends")
                .child(mFirebaseUser.getUid()).child("friendish");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {


                    friends.add(ds.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
//
//    private void searchForPicturesOfFriendsAndItself() {
//
//        for (String id : friends) {
//            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Friends")
//                    .child(id).child("pictures");
//            reference.addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//
//                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                        Picture picture = ds.getValue(Picture.class);
//                        pictures.add(picture);
//                    }
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//        }
//
//   }
}
