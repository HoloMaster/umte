package com.example.umtenew.utils;

import android.util.Log;

import com.example.umtenew.model.PicturesForMap;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.umtenew.HomeActivity.friends;
import static com.example.umtenew.HomeActivity.pictures;


public class SearchFriends {

    public static List<PicturesForMap> picturesForMaps = new ArrayList<>();

    public static void searchForFriends(FirebaseUser mFirebaseUser) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Friends")
                .child(mFirebaseUser.getUid()).child("friendish");
        if (friends != null) {
            friends.clear();
        }
        friends.add(mFirebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {


                    friends.add(ds.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public static void searchForPicturesOfFriendsAndItself() {

        if (pictures != null) {
            pictures.clear();
            picturesForMaps.clear();
        }
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Friends");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    PicturesForMap picturesForMap = new PicturesForMap();
                    picturesForMap.setIdOfFriend(ds.getKey());
                    Picture picture = ds.child("pictures").getValue(Picture.class);

                    picturesForMap.setPicturesNew(picture);

                    if (picture != null) {
                        picturesForMaps.add(picturesForMap);
                    }

                }

                SearchFriends.assign();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public static void assign() {

        for (String id : friends) {
            for (PicturesForMap p : picturesForMaps) {
                if (id.equals(p.getIdOfFriend())) {
                    pictures.add(p.getPicturesNew());
                }
            }

        }
    }
}
