package com.example.umtenew.utils;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.umtenew.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class UserViewHolder extends RecyclerView.ViewHolder {


    TextView userEmail;
    Button addOrRemoveFriend;

    public UserViewHolder(@NonNull View itemView) {
        super(itemView);

        userEmail = itemView.findViewById(R.id.userEmailForAdd);
        addOrRemoveFriend = itemView.findViewById(R.id.addOrRemoveFriend);
    }


}
