package com.example.umtenew.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class CustomMarker implements ClusterItem {

    private final LatLng mPosition;
    private final String mTitle;
    private final String mUrl;



    public CustomMarker(LatLng mPosition, String mTitle, String mUrl) {
        this.mPosition = mPosition;
        this.mTitle = mTitle;
        this.mUrl = mUrl;
    }


    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mUrl;
    }
}
