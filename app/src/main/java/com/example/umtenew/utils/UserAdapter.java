package com.example.umtenew.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.umtenew.FragmentThird;
import com.example.umtenew.R;
import com.example.umtenew.model.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;


public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {

    Context mContext;
    List<User> users;

    FirebaseUser mFirebaseUser;

    public UserAdapter(Context mContext, List<User> users) {
        this.mContext = mContext;
        this.users = users;

    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_layout, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserViewHolder holder, int position) {
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();




        final User user = users.get(position);


        holder.userEmail.setText(user.getEmail());

        isFriend(user.getId(), holder.addOrRemoveFriend);


        if (user.getId().equals(mFirebaseUser.getUid())) {
            holder.addOrRemoveFriend.setVisibility(View.GONE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = mContext.getSharedPreferences("FRIENDS", Context.MODE_PRIVATE).edit();
                editor.putString("profileid", user.getId());
                editor.apply();

                ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new FragmentThird()).commit();


            }

        });

        holder.addOrRemoveFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.addOrRemoveFriend.getText().toString().equals("Add")) {
                    FirebaseDatabase.getInstance().getReference("Friends").child(mFirebaseUser.getUid())
                            .child("friendish").child(user.getId()).setValue(true);
                } else {
                    FirebaseDatabase.getInstance().getReference("Friends").child(mFirebaseUser.getUid())
                            .child("friendish").child(user.getId()).removeValue();

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    private void isFriend(final String id, final Button button) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Friends")
                .child(mFirebaseUser.getUid()).child("friendish");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.child(id).exists()) {
                    button.setText("Remove");

                } else {
                    button.setText("Add");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



}
