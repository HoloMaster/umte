package com.example.umtenew.utils;

import com.example.umtenew.R;

public class TranslateWeather {

    public static String getTranslatedToCz(Weather weather){
        if (weather.icon == 800){
            return "Jasno";
        }
        final int iconId = (int)Math.floor(weather.icon/100);
        switch (iconId){
            case 2:
                return "Bouře";
            case 3:
                return "Jemný déšť";
            case 5:
                return "Déšť";
            case 6:
                return "Sněžení";
            case 7:
                return "Mlhy/Smog";
            case 8:
                return "Polojasno";
            default:
                return "N/A";

        }

    }


}
