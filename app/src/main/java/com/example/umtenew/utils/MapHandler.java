package com.example.umtenew.utils;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.example.umtenew.HomeActivity.friends;
import static com.example.umtenew.HomeActivity.pictures;

public class MapHandler {

    private void searchForFriends(String uuid) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Friends")
                .child(uuid).child("friendish");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {


                    friends.add(ds.getKey());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void searchForPicturesOfFriendsAndItself() {

        for (String id : friends) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Friends")
                    .child(id).child("pictures");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Picture picture = ds.getValue(Picture.class);
                        pictures.add(picture);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }


}
