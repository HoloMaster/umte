package com.example.umtenew.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {
    SharedPreferences sharedPreferences;


    public Prefs(Context c) {
        sharedPreferences = c.getSharedPreferences("filename",Context.MODE_PRIVATE);
    }

    public void setNightMode(Boolean b){
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putBoolean("NightMode",b);
        e.commit();
    }

    public Boolean loadNightMode(){
        Boolean state = sharedPreferences.getBoolean("NightMode",false);
        return state;
    }
}
