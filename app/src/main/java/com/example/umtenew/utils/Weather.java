package com.example.umtenew.utils;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Weather extends AsyncTask<String,Void,String> {


    String weatherIconDesc = "";
    String town = "";
    Integer degrees = 0;
    Integer icon = 0;
    double [] coords;



    @Override
        protected String doInBackground(String... address) {
        try {
            URL url = new URL(address[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            int dataWeather = isr.read();
            String content = "";
            char ch;
            while (dataWeather != -1){
                ch = (char) dataWeather;
                content = content + ch;
                dataWeather = isr.read();

            }
            return content;


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getWeatherIconDesc() {
        return weatherIconDesc;
    }

    public String getTown() {
        return town;
    }

    public Integer getDegrees() {
        return degrees;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public void setDegrees(Integer degrees) {
        this.degrees = degrees;
    }

    public void setWeatherIconDesc(String weatherIconDesc) {
        this.weatherIconDesc = weatherIconDesc;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public double[] getCoords() {
        return coords;
    }

    public void setCoords(double[] coords) {
        this.coords = coords;
    }
}
