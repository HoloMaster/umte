package com.example.umtenew.utils;

import com.example.umtenew.R;

public class WeatherIconResolver {


    public static int getIconsForCurrWeather(final Integer id){
        if (id == 800){
            return R.drawable.ic_wi_day_sunny;
        }
        final int iconId = (int)Math.floor(id/100);
        switch (iconId){
            case 2:
                return R.drawable.ic_wi_day_thunderstorm;
            case 3:
                return R.drawable.ic_wi_day_sprinkle;
            case 5:
                return R.drawable.ic_wi_day_rain;
            case 6:
                return R.drawable.ic_wi_day_snow;
            case 7:
                return R.drawable.ic_wi_fog;
            case 8:
                return R.drawable.ic_wi_day_sunny_overcast;
            default:
                return R.drawable.ic_wi_na;

        }

    }

    public static int getIconsForCurrWeatherForSticker(final String weather){
        if (weather.equals("Jasno")){
            return R.drawable.ic_wi_day_sunny;
        }

        switch (weather){
            case "Bouře":
                return R.drawable.ic_wi_day_thunderstorm;
            case "Jemný déšť":
                return R.drawable.ic_wi_day_sprinkle;
            case "Déšť":
                return R.drawable.ic_wi_day_rain;
            case "Sněžení":
                return R.drawable.ic_wi_day_snow;
            case "Mlhy/Smog":
                return R.drawable.ic_wi_fog;
            case "Polojasno":
                return R.drawable.ic_wi_day_sunny_overcast;
            default:
                return R.drawable.ic_wi_na;

        }

    }
}
