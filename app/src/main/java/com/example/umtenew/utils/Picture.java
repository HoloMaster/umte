package com.example.umtenew.utils;

public class Picture {

    String fileName;
    double latitude;
    double longLatitude;
    String url;
    String author;

    public Picture(String fileName, double latitude, double longLatitude, String url, String author) {
        this.fileName = fileName;
        this.latitude = latitude;
        this.longLatitude = longLatitude;
        this.url = url;
        this.author = author;
    }


    public Picture() {
    }

    public String getFileName() {
        return fileName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongLatitude() {
        return longLatitude;
    }

    public String getUrl() {
        return url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongLatitude(double longLatitude) {
        this.longLatitude = longLatitude;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "fileName='" + fileName + '\'' +
                ", latitude=" + latitude +
                ", longLatitude=" + longLatitude +
                ", url='" + url + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
