package com.example.umtenew;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.umtenew.model.PicturesForMap;
import com.example.umtenew.utils.Picture;
import com.example.umtenew.utils.Prefs;
import com.example.umtenew.utils.SearchFriends;
import com.example.umtenew.utils.Weather;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.example.umtenew.utils.SearchFriends.picturesForMaps;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    Toolbar toolbar;
    NavigationView navigationView;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private FusedLocationProviderClient locationClient;
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    Location locationGPS;
    LocationManager locationManager;
    double[] locationsFromGps = new double[2];
    private Location locationNET;
    private SharedPreferences sharedPreferences;
    private Prefs prefs;
    boolean authWithFingerprint;


    public static List<String> friends = new ArrayList<>();
    public static List<Picture> pictures = new ArrayList<>();

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        FirebaseUser user = mFirebaseAuth.getCurrentUser();

        prefs = new Prefs(this);


        if (user == null) {
            sendUserToLogin();
        }


    }

    private void sendUserToLogin() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

       SearchFriends.searchForFriends(mFirebaseUser);

        Log.d("PRATELE", ""+friends.size());

        if (friends != null && !friends.isEmpty()) {

            SearchFriends.searchForPicturesOfFriendsAndItself();


        }

        for (String id:friends) {
            for (PicturesForMap p : picturesForMaps) {
                if (id.equals(p.getIdOfFriend())){
                    pictures.add(p.getPicturesNew());
                    Log.d("PRATELE", p.getPicturesNew().toString());
                }
            }

        }




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        sharedPreferences = getApplicationContext().getSharedPreferences("Preferences", 0);


        authWithFingerprint = sharedPreferences.getBoolean("LOGIN", false);


        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.navbar);
        navigationView.setNavigationItemSelectedListener(this);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open, R.string.close);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();

        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.loginWithFinger);

        if (!authWithFingerprint) {

            menuItem.setTitle(R.string.zapnout_overeni_otiskem);

        } else {

            menuItem.setTitle(R.string.vypnout_overeni_otiskem);

        }
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.fragmentContainer, new MainFragment());
        fragmentTransaction.commit();

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        drawerLayout.closeDrawer(GravityCompat.START);
        if (item.getItemId() == R.id.home) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.fragmentContainer, new MainFragment());
            fragmentTransaction.commit();
        }
        if (item.getItemId() == R.id.contacts) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.fragmentContainer, new FragmentThird());
            fragmentTransaction.commit();

        }
        if (item.getItemId() == R.id.map) {

//
//            SearchFriends.searchForFriends(mFirebaseUser);
//
//            Log.d("PRATELE", ""+friends.size());
//
//            if (friends != null && !friends.isEmpty()) {
//
//                    SearchFriends.searchForPicturesOfFriendsAndItself();
//
//
//            }

           Log.d("PRATELE", ""+pictures.size());






            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.fragmentContainer, new FragmentSecond());
            fragmentTransaction.commit();

        }
        if (item.getItemId() == R.id.logout) {
            mFirebaseAuth.signOut();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("LOGIN", false);
            editor.apply();
            sendUserToLogin();
        }

        if (item.getItemId() == R.id.loginWithFinger) {




            if (!authWithFingerprint) {

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("LOGIN", true);
                editor.apply();
                authWithFingerprint = true;

                item.setTitle(R.string.zapnout_overeni_otiskem);


            } else {

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("LOGIN", false);
                editor.apply();
                authWithFingerprint = false;
                item.setTitle(R.string.vypnout_overeni_otiskem);

            }


        }


        return true;
    }

    public Weather getWeather(double latitude, double longLatitude) {
        String content;
        Weather weather = new Weather();
        try {
            Log.d("HOMEACTIVITY", latitude + " " + longLatitude);
            content = weather.execute("https://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longLatitude + "&appid=f1b60a7ce5ed4bd96e68bb076b45583f&units=metric&lang=cz").get();

            JSONObject objectJ = new JSONObject(content);
            String weatherFromJson = objectJ.getString("weather");
            JSONArray array = new JSONArray(weatherFromJson);


            String weatherFromJson2 = objectJ.getString("main");
            JSONObject objectDegrees = new JSONObject(weatherFromJson2);
            Integer degrees = objectDegrees.getInt("temp");


            String town = objectJ.getString("name");


            String weatherIconDesc = "";
            Integer icon = 0;


            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                weatherIconDesc = object.getString("main");
                icon = object.getInt("id");

            }


            weather.setIcon(icon);
            weather.setWeatherIconDesc(weatherIconDesc);
            weather.setTown(town);
            weather.setDegrees(degrees);


        } catch (Exception e) {
            Toast.makeText(this, "Behem ziskavani dat pcoasi doslo k chybe.", Toast.LENGTH_SHORT).show();
            Log.d("HOMEACTIVITY", e.toString());
        }
        return weather;
    }

    public double[] getLocation() {

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationServices.getFusedLocationProviderClient(HomeActivity.this).requestLocationUpdates(
                locationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(HomeActivity.this)
                                .removeLocationUpdates(this);
                        if (locationResult != null && locationResult.getLocations().size() > 0) {
                            int latestLocationIndex = locationResult.getLocations().size() - 1;
                            locationsFromGps[0] =
                                    locationResult.getLocations().get(latestLocationIndex).getLatitude();
                            locationsFromGps[1] =
                                    locationResult.getLocations().get(latestLocationIndex).getLongitude();

                        }
                    }
                }, Looper.getMainLooper());

        if (locationsFromGps[0] == 0.0 && locationsFromGps[1] == 0.0) {
            locationManager =
                    (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                Toast.makeText(this, "Apliakci neni povolen pristup k poloze", Toast.LENGTH_SHORT).show();
            }
            locationNET = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            locationsFromGps[0] =
                    locationNET.getLatitude();
            locationsFromGps[1] =
                    locationNET.getLongitude();
        }

        return locationsFromGps;
    }



}
