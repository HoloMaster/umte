package com.example.umtenew;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.umtenew.model.User;
import com.example.umtenew.utils.UserAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FragmentThird extends Fragment {


    FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    FloatingActionButton addFriendActivity;
    FirebaseAuth firebaseAuth;
    ImageButton searchButton;
    EditText searchFriend;
    ConstraintLayout constraintLayout;


    private final String userId = mFirebaseUser.getUid();

    RecyclerView friendList;

    DatabaseReference databaseReference;

    private List<User> users;
    private UserAdapter userAdapter;
    private List<String> usersId = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_third,container,false);

        addFriendActivity = view.findViewById(R.id.addFriendActivity);
        constraintLayout = view.findViewById(R.id.linearLayout);


        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference("users");
        searchButton = view.findViewById(R.id.searchButton);
        friendList = view.findViewById(R.id.recycleView);
        searchFriend = view.findViewById(R.id.searchFriend);

        addFriendActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constraintLayout.setVisibility(View.VISIBLE);
            }
        });


        friendList.setHasFixedSize(false);
        friendList.setLayoutManager(new LinearLayoutManager(getContext()));

        users = new ArrayList<>();




        userAdapter = new UserAdapter(getContext(),users);
        friendList.setAdapter(userAdapter);

        initialiseFreindList();

        searchFriend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchUser(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        return view;
    }

    private void searchUser(String s){
        Query query = FirebaseDatabase.getInstance().getReference("users").orderByChild("userName")
                .startAt(s)
                .endAt(s+"\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                users.clear();
                for (DataSnapshot snapshot:
                        dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    if (!user.getId().equals(userId)){
                    users.add(user);
                    }
                }

                userAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void initialiseFreindList(){
        Query query = FirebaseDatabase.getInstance().getReference("Friends").child(firebaseAuth.getUid()).child("friendish");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersId.clear();
                for (DataSnapshot snapshot:
                        dataSnapshot.getChildren()) {

                    String userid = snapshot.getKey();
                    usersId.add(userid);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Query query1 = FirebaseDatabase.getInstance().getReference("users");

        query1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                users.clear();
                for (int i = 0; i < usersId.size(); i++) {
                    for (DataSnapshot d:
                         dataSnapshot.getChildren()) {
                        if (usersId.get(i).equals(d.getKey())){
                            User user = d.getValue(User.class);
                            users.add(user);

                        }
                    }

                }
                userAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
