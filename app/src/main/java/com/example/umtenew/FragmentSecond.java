package com.example.umtenew;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.umtenew.utils.TranslateWeather;
import com.example.umtenew.utils.Weather;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.StorageReference;

import java.util.Collection;
import java.util.Random;

import static com.example.umtenew.HomeActivity.friends;
import static com.example.umtenew.HomeActivity.pictures;

public class FragmentSecond extends Fragment implements OnMapReadyCallback {

    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private MapView mapView;
    private GoogleMap map;
    private double[] loc;
    private ProgressBar progressBar;

    private StorageReference mStorageRef;


    @Override
    public void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.CAMERA}, 100);
        }


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        mapView = view.findViewById(R.id.mapView);


        HomeActivity activity = (HomeActivity) getActivity();

        loc = activity.getLocation();

        final Weather weather = activity.getWeather(loc[0], loc[1]);

        FloatingActionButton btnCamera = view.findViewById(R.id.cameraButton);
        FloatingActionButton centerGPS = view.findViewById(R.id.centerGPS);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(getActivity(), CameraActivity.class);
                    intent.putExtra("CITY", weather.getTown());
                    intent.putExtra("DEGREE", weather.getDegrees().toString());
                    intent.putExtra("WEATHER", (TranslateWeather.getTranslatedToCz(weather)));
                    intent.putExtra("LOC", loc);
                    startActivity(intent);
                } else {

                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, 100);

                }
            }
        });

        centerGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LatLng latLng = new LatLng(loc[0], loc[1]);
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 9));

            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(loc[0], loc[1]);

        for (int i = 0; i < pictures.size(); i++) {
            LatLng latLngForPicture = null;


            if (i < pictures.size() - 1) {
                if (pictures.get(i).getLatitude() == pictures.get(i + 1).getLatitude() && pictures.get(i + 1).getLongLatitude() == pictures.get(i + 1).getLongLatitude()) {

                    latLngForPicture = new LatLng(pictures.get(i).getLatitude() + getOffset(), pictures.get(i).getLongLatitude() + getOffset());

                } else {

                    latLngForPicture = new LatLng(pictures.get(i).getLatitude(), pictures.get(i).getLongLatitude());
                }
            }else{

                latLngForPicture = new LatLng(pictures.get(i).getLatitude(), pictures.get(i).getLongLatitude());
            }


            Marker marker = googleMap.addMarker(new MarkerOptions()
                    .position(latLngForPicture)
                    .title(pictures.get(i).getAuthor()));
            marker.setTag(pictures.get(i).getUrl());
        }

        map = googleMap;

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                String markerTitle = marker.getTitle();
                String pictureUrl = (String) marker.getTag();

                Intent intent = new Intent(getContext(), PictureView.class);

                intent.putExtra("title", markerTitle);
                intent.putExtra("url", pictureUrl);

                startActivity(intent);


                return false;
            }
        });
        MapsInitializer.initialize(getContext());
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        progressBar.setVisibility(View.GONE);


    }

    private double getOffset() {
        Random r = new Random();
        double randomValue = 0.0002f + (0.0004f - 0.0002f) * r.nextDouble();
        return randomValue;
    }




}
