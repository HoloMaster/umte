package com.example.umtenew.model;

import com.example.umtenew.utils.Picture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PicturesForMap {

    String idOfFriend;
    Picture picturesNew;


    public PicturesForMap(String idOfFriend, Picture picturesNew) {
        this.idOfFriend = idOfFriend;
        this.picturesNew = picturesNew;
    }

    public PicturesForMap(){}



    public String getIdOfFriend() {
        return idOfFriend;
    }



    public void setIdOfFriend(String idOfFriend) {
        this.idOfFriend = idOfFriend;
    }

    public Picture getPicturesNew() {
        return picturesNew;
    }

    public void setPicturesNew(Picture picturesNew) {
        this.picturesNew = picturesNew;
    }

    @Override
    public String toString() {
        return "PicturesForMap{" +
                "idOfFriend='" + idOfFriend + '\'' +
                ", picturesNew=" + picturesNew +
                '}';
    }
}
