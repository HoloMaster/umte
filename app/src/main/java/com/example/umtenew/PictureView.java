package com.example.umtenew;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;

public class PictureView extends AppCompatActivity {

    private DatabaseReference databaseReference;

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_view);

        imageView = findViewById(R.id.pictureOfWeather);

        String url = getIntent().getStringExtra("url");

        if (url != null){

            Picasso.get()
                    .load(url)
                    .into(imageView);
        }




    }
}
