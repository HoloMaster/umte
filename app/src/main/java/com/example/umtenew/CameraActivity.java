package com.example.umtenew;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.net.Uri;
import android.os.Bundle;

import com.example.umtenew.utils.Prefs;
import com.example.umtenew.utils.SearchFriends;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.xiaopo.flying.sticker.BitmapStickerIcon;
import com.xiaopo.flying.sticker.DeleteIconEvent;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.FlipHorizontallyEvent;
import com.xiaopo.flying.sticker.Sticker;
import com.xiaopo.flying.sticker.StickerView;
import com.xiaopo.flying.sticker.TextSticker;
import com.xiaopo.flying.sticker.ZoomIconEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.Layout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static com.example.umtenew.HomeActivity.friends;
import static com.example.umtenew.utils.WeatherIconResolver.getIconsForCurrWeatherForSticker;

public class CameraActivity extends AppCompatActivity {


    FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    DatabaseReference ref;
    FirebaseDatabase database;



    ImageView imageView;
    StickerView stickerView;
    TextSticker textSticker, textSticker2;
    ImageButton imageButton, imageButton2, imageButton3;
    FloatingActionButton sendPicture, cancelPicture;
    private Prefs prefs;
    private StringBuilder stickerContent;
    String currentPhotoPath;
    private StorageReference mStorageRef;
    private final String userId = mFirebaseUser.getEmail();
    String imageFileName;
    boolean activityAlreadyCreated = false;
    Map<String, Object> picture = new HashMap<>();
    private SharedPreferences sharedPreferences;
    private boolean authWithFingerprint;

    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        sharedPreferences = getApplicationContext().getSharedPreferences("Preferences", 0);


        authWithFingerprint = sharedPreferences.getBoolean("LOGIN", false);


        mStorageRef = FirebaseStorage.getInstance().getReference();

        imageView = findViewById(R.id.imageViewFromCam);
        stickerView = findViewById(R.id.stickerView);
        imageButton = findViewById(R.id.imageButton);
        imageButton2 = findViewById(R.id.imageButton2);
        imageButton3 = findViewById(R.id.imageButton3);
        sendPicture = findViewById(R.id.sendPicture);
        cancelPicture = findViewById(R.id.cancelPicture);


        //default icon layout
        stickerView.configDefaultIcons();

        stickerView.setBackgroundColor(Color.WHITE);
        stickerView.setLocked(false);
        stickerView.setConstrained(true);
        initTextSticker();

        if (ContextCompat.checkSelfPermission(CameraActivity.this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CameraActivity.this, new String[]{
                    Manifest.permission.CAMERA}, 100);
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 100);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stickerView.removeAllStickers();
                initTextSticker();
                stickerView.addSticker(textSticker);
            }
        });

        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stickerView.removeAllStickers();
                initIconSticker();
                stickerView.addSticker(textSticker);
//                stickerView.addSticker(textSticker2);

            }
        });
        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stickerView.removeAllStickers();
                initIconStickerAndText();
                stickerView.addSticker(textSticker);

            }
        });


        sendPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stickerView.setIcons(new ArrayList<BitmapStickerIcon>());
                Bitmap bitmap = getBitmapFromView(stickerView);

                FileOutputStream outStream = null;
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                imageFileName = "JPEG_" + timeStamp + "_";

                File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                storageDir.mkdirs();
                File outFile = new File(storageDir, imageFileName);

                currentPhotoPath = outFile.getAbsolutePath();
                try {
                    outStream = new FileOutputStream(outFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                    outStream.flush();
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Uri file = Uri.fromFile(new File(currentPhotoPath));
                final StorageReference riversRef = mStorageRef.child("images/" + imageFileName + ".jpg");




                riversRef.putFile(file)

                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // Get a URL to the uploaded content
                                Task<Uri> downloadUrl = taskSnapshot.getStorage().getDownloadUrl();

                                riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {

                                        final double[] loc = getIntent().getDoubleArrayExtra("LOC");
                                        if (loc != null) {

                                            picture.put("fileName", imageFileName);
                                            picture.put("latitude", loc[0]);
                                            picture.put("longLatitude", loc[1]);
                                            picture.put("url", uri.toString());
                                            picture.put("author", userId);

                                            FirebaseDatabase.getInstance().getReference("Friends").child(mFirebaseUser.getUid())
                                                    .child("pictures").removeValue();

                                            FirebaseDatabase.getInstance().getReference("Friends").child(mFirebaseUser.getUid())
                                                    .child("pictures").setValue(picture)
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            Intent intent = new Intent(CameraActivity.this, HomeActivity.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    });


                                        }

                                    }
                                });


                                Toast.makeText(CameraActivity.this, "Nahravani uspesne", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    }
                });


            }
        });

        cancelPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recreate();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            Bitmap caBitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(caBitmap);
            stickerView.addSticker(textSticker);

        }


    }

    public String initializeWeatherDesc() {
        final String city = getIntent().getStringExtra("CITY");

        final String degree = getIntent().getStringExtra("DEGREE") + " °C";

        final String weather = getIntent().getStringExtra("WEATHER");

        stickerContent = new StringBuilder().append(city + "\n")
                .append(degree + "\n")
                .append(weather + "\n");

        return stickerContent.toString();


    }

    public String initializeWeatherDescWithIcon() {

        final String degree = getIntent().getStringExtra("DEGREE") + " °C";


        stickerContent = new StringBuilder()
                .append(degree + "\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n")
                .append("\n");

        return stickerContent.toString();


    }

    public void initTextSticker() {

        textSticker = new TextSticker(CameraActivity.this);
        textSticker.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_wi_backgroundcolor_for_sticker));
        textSticker.setTextColor(Color.WHITE);
        textSticker.setText(initializeWeatherDesc());
        textSticker.setTextAlign(Layout.Alignment.ALIGN_CENTER);
        textSticker.resizeText();
    }

    public void initIconSticker() {


        Drawable icon = ContextCompat.getDrawable(this, getIconsForCurrWeatherForSticker(getIntent().getStringExtra("WEATHER")));


        icon.setTint(Color.WHITE);

        textSticker = new TextSticker(CameraActivity.this);
        textSticker.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_wi_backgroundcolor_for_sticker));
        textSticker.setTextColor(Color.WHITE);
        textSticker.setText(initializeWeatherDescWithIcon());
        textSticker.setDrawable(icon);
        textSticker.setTextAlign(Layout.Alignment.ALIGN_NORMAL);
        textSticker.resizeText();
    }

    public void initIconStickerAndText() {


        Drawable icon = ContextCompat.getDrawable(this, getIconsForCurrWeatherForSticker(getIntent().getStringExtra("WEATHER")));

        icon.setTint(Color.WHITE);

        textSticker = new TextSticker(CameraActivity.this);
        textSticker.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_wi_backgroundcolor_for_sticker));
        textSticker.setTextColor(Color.WHITE);
        textSticker.setText(initializeWeatherDesc());
        stickerView.addSticker(new DrawableSticker(icon));
        textSticker.setTextAlign(Layout.Alignment.ALIGN_CENTER);
        textSticker.resizeText();
    }

    public static Bitmap getBitmapFromView(View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return returnedBitmap;
    }

}
